import { Component, OnInit } from '@angular/core';
import {trigger,state, style, animate,query, stagger, transition} from '@angular/animations';
import { InsertRemoveComponent } from '../insert-remove/insert-remove.component';

@Component({
  selector: 'app-hero-list-page',
  animations: [
    trigger('filterAnimation', [
      transition(':enter, * => 0, * => -1', []),
      transition(':increment', [
        query(':enter', [
          style({ opacity: 0, width: '0px' }),
          stagger(50, [
            animate('300ms ease-out', style({ opacity: 1, width: '*' })),
          ]),
        ], { optional: true })
      ]),
      transition(':decrement', [
        query(':leave', [
          stagger(50, [
            animate('300ms ease-out', style({ opacity: 0, width: '0px' })),
          ]),
        ])
      ]),
    ]),

  ],
  templateUrl: './hero-list-page.component.html',
  styleUrls: ['./hero-list-page.component.css']
})
export class HeroListPageComponent implements OnInit {

  heroes: any[] = [
    { id: 11, name: 'Dr Nice' },
    { id: 12, name: 'Narco' },
    { id: 13, name: 'Bombasto' },
    { id: 14, name: 'Celeritas' },
    { id: 15, name: 'Magneta' },
    { id: 16, name: 'RubberMan' },
    { id: 17, name: 'Dynama' },
    { id: 18, name: 'Dr IQ' },
    { id: 19, name: 'Magma' },
    { id: 20, name: 'Tornado' }
  ];
  nbHeroes: number = 0;
  constructor() {
  }


  removeHero(rank) {
    this.heroes.splice(rank, 1);
  }

  addHeroes(){
    this.nbHeroes = this.nbHeroes + 1;
  }
  ngOnInit(): void {
  }


}
