import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InsertRemoveComponent } from './composants/insert-remove/insert-remove.component';
import { HeroListPageComponent } from './composants/hero-list-page/hero-list-page.component';
import { HeroListEnterLeaveComponent } from './composants/hero-list-enter-leave/hero-list-enter-leave.component';


const routes: Routes = [
  {path: 'insertremove', component: InsertRemoveComponent},
  {path: 'heropage', component: HeroListPageComponent},
  {path: 'bouge', component: HeroListEnterLeaveComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
