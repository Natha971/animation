import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OpenCloseComponent } from './composants/open-close/open-close.component';
import { HeroListEnterLeaveComponent } from './composants/hero-list-enter-leave/hero-list-enter-leave.component';
import { InsertRemoveComponent } from './composants/insert-remove/insert-remove.component';
import { HeroListPageComponent } from './composants/hero-list-page/hero-list-page.component';
import { AnimationComponent } from './composants/animation/animation.component';


@NgModule({
  declarations: [
    AppComponent,
    OpenCloseComponent,
    HeroListEnterLeaveComponent,
    InsertRemoveComponent,
    HeroListPageComponent,
    AnimationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
