import { Component } from '@angular/core';
import { slideInAnimation } from './composants/animation/animation.component';

@Component({
  selector: 'app-root',
  animations: [
    slideInAnimation
    // animation triggers go here
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'testanim';
}
